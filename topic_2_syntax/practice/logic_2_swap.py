def swap(first_param, second_param):
    """
    Функция swap.

    Принимает 2 аргумента (first_param, second_param) с некоторыми значениями.
    Поменять местами значения этих переменных и
    вывести на экран таким образом "first_param = 3 | second_param = True".
    """

    # классический swap
    # tmp = first_param
    # first_param = second_param
    # second_param = tmp

    first_param, second_param = second_param, first_param   # по-питоновски :)

    print(f'first_param = {first_param} | second_param = {second_param}')


if __name__ == '__main__':
    swap(1, '555')
