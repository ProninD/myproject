"""
Ввод данных с клавиатуры:
- Имя
- Фамилия
- Возраст

Вывод данных в формате:
"Вас зовут <Имя> <Фамилия>! Ваш возраст равен <Возраст>."
"""
name = input("what is your name:")
surname = input("what is your surname:")
age = input("how old are you:")

print(f"Вас зовут {name} {surname}! Ваш возраст равен {age}.")

